package module1Assignment;

/**
 * 
 * @author David Rios 
 * Date: 1/10/19 
 * Program Name: Rios_Application
 * Purpose: Simulation using "call" button, call attendant
 */
public class Rios_Application 
{
	
	public static void main(String[] args) 
	{
		Rios_Airplane airplane = new Rios_Airplane();
		airplane.menu();
	}

}
