package module1Assignment;

import java.util.Arrays;
import java.util.Scanner;

/**
 * 
 * @author David Rios Date: 1/10/19 
 * Program Name: Rios_Airplane 
 * Purpose: Simulation using "call" button, call attendant
 *
 */
public class Rios_Airplane 
{

	public Rios_Airplane() 
	{
		initialize();
	}

	public void initialize() 
	{
		this.m_seats = new String[5];
		for (int i = 0; i < m_seats.length; i++) 
		{
			m_seats[i] = STATE_OFF;
		}
	}

	/**
	 * @return the m_seats
	 */
	public String[] getSeats() 
	{
		return m_seats;
	}

	/**
	 * @param m_seats
	 *            the Index of seat to set
	 */
	public void setSeats(int seat, String state) 
	{
		m_seats[seat] = state;
	}

	// Method for turning on specific seat
	public void switchSeatOn(int seatNum) 
	{
		int arrayNum = seatNum - 1;
		switch (seatNum) 
		{
		case 1:
			setSeats(arrayNum, STATE_ON);
			break;
		case 2:
			setSeats(arrayNum, STATE_ON);
			break;
		case 3:
			setSeats(arrayNum, STATE_ON);
			break;
		case 4:
			setSeats(arrayNum, STATE_ON);
			break;
		case 5:
			setSeats(arrayNum, STATE_ON);
			break;
		}
	}

	// Flight Attendant Off switch
	public void switchAll(int num) 
	{
		for (int i = 0; i < m_seats.length; i++) 
		{
			this.setSeats(i, STATE_OFF);
		}
	}
	
	// User interface Menu. Uses method recursively to create a active "state
		// machine" of sorts.
	public void menu() 
	{
		Scanner scanner = new Scanner(System.in);

		// Main greeting and prints contents of array
		System.out.println("Enter choice: (1) Call Light Reset (All) (2) Call Attendant (3) Exit\r" );
		System.out.println("=========================================================================\r");
		System.out.println(Arrays.toString(getSeats()) + "\r");

		int choice = scanner.nextInt();

		// main switch which corresponds to the main menu contents
		switch (choice) 
		{

		case 1:
			switchAll(choice);
			menu();
			break;
		// Sub Menu for Call Attendant Light.
		case 2:
				System.out.println("Please select a seat to turn off");
				int seatChoice = scanner.nextInt();

				switch (seatChoice) 
				{
				case 1:
					switchSeatOn(seatChoice);
					menu();
					scanner.nextInt();
					break;
				case 2:
					System.out.println("Seat " + seatChoice + " light is on");
					switchSeatOn(seatChoice);
					menu();
					scanner.nextInt();
					break;
				case 3:
					System.out.println("Seat" + seatChoice + "light is on");
					switchSeatOn(seatChoice);
					menu();
					scanner.nextInt();
					break;
				case 4:
					System.out.println("Seat" + seatChoice + "light is on");
					switchSeatOn(seatChoice);
					menu();
					scanner.nextInt();
					break;
				case 5:
					System.out.println("Seat" + seatChoice + "light is on");
					switchSeatOn(seatChoice);
					System.out.println(Arrays.toString(getSeats()));
					menu();
					scanner.nextInt();
					break;
				}
		case 3:
			System.exit(3);
		default:
			System.out.println("Incorrect Menu Choice. Please select from a number");
			menu();
		}
	}
	
	// Member Variables and constants
	private String[] m_seats;
	private String STATE_ON = "ON";
	private String STATE_OFF = "OFF";

}
