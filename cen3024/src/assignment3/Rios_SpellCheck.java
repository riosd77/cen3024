
package assignment3;

import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

/**
 * 
 * @author David Rios
 * Date: 1/24/2019
 * Program Name: Spell_Check
 * Purpose: Spell check file against dictionary
 *
 */
public class Rios_SpellCheck {
	
	// Constructors
	public Rios_SpellCheck() {
		
	}

	// Getters and Setters
	
	/**
	 * @return the arr
	 */
	public ArrayList<String> getArr() {
		return arr;
	}

	/**
	 * Sets member var arr 
	 * @param arr the ArrayList to set
	 */
	public void setArr(ArrayList<String> arr) {
		this.arr = arr;
	}

	//Methods
	
	/**
	 * Description: method 
	 *@return tempArray ArrayList built from passed filename
	 */
	public ArrayList<String> readFileAsString(String fileName) {
		
		ArrayList<String> tempArray = getArr();
		
		try {
			URI uri = this.getClass().getResource(fileName).toURI();
			tempArray = (ArrayList<String>) Files.readAllLines(Paths.get(uri)); // converts fileName to path per OS
		} catch (Exception e) {
			e.printStackTrace();
			}
		return tempArray;	
	}
	
	// Fields - Member Variables
	private ArrayList<String> arr; 
		
}
