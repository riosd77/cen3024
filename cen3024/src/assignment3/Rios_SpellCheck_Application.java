package assignment3;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * 
 * @author David Rios
 * Date: 1/24/2019
 * Program Name: SpellCheck_Application
 * Purpose: Spell check file against dictionary
 *
 */
public class Rios_SpellCheck_Application {

	public static void main(String[] args) {
		
		// initialize Objects
		Rios_SpellCheck spell = new Rios_SpellCheck();
		Scanner sc = new Scanner(System.in); 
		ArrayList<String> file = new ArrayList<String>(); 	
		ArrayList<String> dict = new ArrayList<String>();
		
		//------Application start-------//
		
		System.out.println("Name of File to Check (ex: FileName.txt):"); 			
		file = spell.readFileAsString(sc.nextLine());
		
		System.out.println("Name of Dictionary to Check Against (ex: FileName.txt):");
		dict = spell.readFileAsString(sc.nextLine());
		
		// Main Loop: processes each word in file and checks to see if it exist in dict
		for (String word : file) {
			if ( !dict.contains(word))
				System.out.println(word + " is an unknown word");
		}
		
		//closes scanner
		sc.close();
	}
}
